package com.rkg.Assignment4;


import java.util.*;

public class ListOfStudent {
    public static void main(String[] args) {
        Student[] students = new Student[10];

        students[0] = new Student("Tony Stark1", 50);
        students[1] = new Student("Tony Stark2", 40);
        students[2] = new Student("Tony Stark3", 39);
        students[3] = new Student("Tony Stark4", 38);
        students[4] = new Student("Tony Stark5", 37);
        students[5] = new Student("Tony Stark6", 51);
        students[6] = new Student("Tony Stark7", 52);
        students[7] = new Student("Tony Stark8", 53);
        students[8] = new Student("Tony Stark9", 54);
        students[9] = new Student("Tony Stark10", 55);

        List<Student> pass = new ArrayList<>();
        List<Student> fail = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (students[i].getMarks() >= 40) {
                pass.add(students[i]);
            } else {
                fail.add(students[i]);
            }
        }

        Map<String, List<Student>> result = new HashMap<>();
        result.put("Pass", pass);
        result.put("Fail", fail);

        for(Map.Entry k : result.entrySet()){
            String key = (String)k.getKey();
            System.out.println(key+" : ");
            List<Student> student = (List<Student>)k.getValue();
            for (int i = 0; i < student.size(); i++) {
                System.out.println(student.get(i).getName());
            }
        }
    }
}
