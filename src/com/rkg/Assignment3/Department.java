package com.rkg.Assignment3;

public class Department {
    String deptname;
    String deptid;
    String depthead;

    public Department(String deptname, String deptid, String depthead) {
        this.deptname = deptname;
        this.deptid = deptid;
        this.depthead = depthead;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getDepthead() {
        return depthead;
    }

    public void setDepthead(String depthead) {
        this.depthead = depthead;
    }




}
