package com.rkg.Assignment3;

import java.util.*;
import java.util.stream.*;

public class SalarySumDeptwise {
    public static void main(String[] args) {
        Department department1 = new Department("DE","1","Hulk");
        Department department2 = new Department("DS","2","CaptainAmerica");
        Department department3 = new Department("DevOps","3","Thor");

        Employee employee1 = new Employee("Tony","Stark",100,department1);
        Employee employee2 = new Employee("Tony","Stark",100,department1);
        Employee employee3 = new Employee("Tony","Stark",100,department1);
        Employee employee4 = new Employee("Tony","Stark",100,department1);
        Employee employee5 = new Employee("Tony","Stark",100,department2);
        Employee employee6 = new Employee("Tony","Stark",100,department2);
        Employee employee7 = new Employee("Tony","Stark",100,department2);
        Employee employee8 = new Employee("Tony","Stark",100,department2);
        Employee employee9 = new Employee("Tony","Stark",100,department3);
        Employee employee10 = new Employee("Tony","Stark",100,department3);
        Employee employee11 = new Employee("Tony","Stark",100,department3);
        Employee employee12 = new Employee("Tony","Stark",100,department3);

        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);
        employeeList.add(employee5);
        employeeList.add(employee6);
        employeeList.add(employee7);
        employeeList.add(employee8);
        employeeList.add(employee9);
        employeeList.add(employee10);
        employeeList.add(employee11);
        employeeList.add(employee12);


        Map<Department,Integer> departmentWiseSalary = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,Collectors.summingInt(Employee::getSalary)));

        for(Department department : departmentWiseSalary.keySet()){
            System.out.println(department.getDeptname()+" "+departmentWiseSalary.get(department));
        }
    }
}
