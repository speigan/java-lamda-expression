package com.rkg.Assignment1;

public abstract class StringUtills implements TwoStringPredicate {

    public static void main(String[] args) {

        String string1="Hulk";
        String string2 = "TonyStark";
        Integer a = string1.length();
        Integer b = string2.length();
        String longer = BetterString(string1,string2,(s1,s2) -> {
            return s1.length() > s2.length();
        });
        String first = BetterString(string1,string2,(s1,s2) -> {
            return true;
        });
        System.out.println(longer);
        System.out.println(first);

    }

    public static String BetterString(String string1, String string2, TwoStringPredicate twoStringPredicate) {
        if(twoStringPredicate.BetterStringMethod(string1,string2)) {
            return string1;
        }
        else return string2;
    }

}


